package ru.t1.nikitushkina.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.api.service.ITokenService;

@Getter
@Setter
@NoArgsConstructor
public class TokenService implements ITokenService {

    @Nullable
    private String token;

}
