package ru.t1.nikitushkina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.dto.request.TaskRemoveByIndexRequest;
import ru.t1.nikitushkina.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-remove-by-index";

    @NotNull
    public static final String DESCRIPTION = "Remove task by index.";

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.print("ENTER ID: ");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        @Nullable final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(getToken());
        request.setIndex(index);
        getTaskEndpoint().removeTaskByIndex(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
