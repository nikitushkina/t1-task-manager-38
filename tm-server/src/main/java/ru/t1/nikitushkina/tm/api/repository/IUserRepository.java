package ru.t1.nikitushkina.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.enumerated.Role;
import ru.t1.nikitushkina.tm.model.User;

import java.sql.ResultSet;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    User add(@NotNull User user) throws Exception;

    @NotNull
    User create(@NotNull String login, @NotNull String password) throws Exception;

    @NotNull
    User create(
            @NotNull String login,
            @NotNull String password,
            @Nullable String email
    ) throws Exception;

    @NotNull
    User create(
            @NotNull String login,
            @NotNull String password,
            @Nullable Role role
    ) throws Exception;

    @NotNull
    User fetch(@NotNull ResultSet row) throws Exception;

    @Nullable
    User findByEmail(@NotNull String email) throws Exception;

    @Nullable
    User findByLogin(@NotNull String login) throws Exception;

    Boolean isEmailExist(@NotNull String email) throws Exception;

    Boolean isLoginExist(@NotNull String login) throws Exception;

    void update(@NotNull User user) throws Exception;

}
