package ru.t1.nikitushkina.tm.exception.user;

public final class ExistEmailException extends AbstractUserException {

    public ExistEmailException() {
        super("Error! Email already exists.");
    }

}
